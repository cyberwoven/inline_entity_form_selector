(function ($, Drupal) {
  Drupal.behaviors.inlineEntityFormSelector = {
    attach: function (context, settings) {
      $('.inline-entity-form-autocomplete-selector', context).each(function () {
        $(this).closest('.fieldset-wrapper').find('.form-autocomplete').css('display', 'none');
      });

      $('.inline-entity-form-autocomplete-selector', context).on('change', function (e) {
        e.preventDefault();
        $(this).closest('.fieldset-wrapper').find('.form-autocomplete').val($(this).val());
      });
    }
  };
})(jQuery, Drupal);
